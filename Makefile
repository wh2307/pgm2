ADD = addqueue.c
SHO = showqueue.c
REM = rmqueue.c

build: spool

spool: addqueue showqueue rmqueue

addqueue: $(ADD)
	gcc -Wall -o addqueue $(ADD)

showqueue: $(SHO)
	gcc -Wall -o showqueue $(SHO)

rmqueue: $(REM)
	gcc -Wall -o rmqueue $(REM)

install: spool
	mkdir wh2307-spool
	sudo chown user01 wh2307-spool
	sudo chmod 700 wh2307-spool
	echo 0 > counter_spool
	sudo chown user01 counter_spool
	sudo chmod 700 counter_spool
	sudo mv wh2307-spool counter_spool /
	sudo chown user01 addqueue showqueue rmqueue
	sudo chmod 111 addqueue showqueue rmqueue
	sudo chmod +s addqueue showqueue rmqueue
	sudo mv addqueue showqueue rmqueue /bin

clean: 
	rm -f *.o a.out *.test *.core addqueue showqueue rmqueue
	rm -rf wh2307-spool

uninstall:
	sudo rm /bin/addqueue /bin/showqueue /bin/rmqueue
	sudo rm -rf /wh2307-spool
	sudo rm /counter_spool

test: gen_test
	addqueue user01_01.test user02_02.test user03_03.test
	addqueue world01_01.test user01_r.test user01_w.test
	addqueue world02_01.test hello.world file_doesnt_exist.ha
	addqueue world03_01.test not_a_file
	showqueue
	sudo setuid 1002 addqueue user01_01.test
	showqueue
	rmqueue 5_user01_01.test
	sudo setuid 1003 rmqueue not_in_queue
	sudo setuid 1003 rmqueue 1_user01_r.test 
	rmqueue 0_world01_01.test
	rmqueue 1_user01_r.test
	rmqueue 2_world02_01.test
	rmqueue 3_world03_01.test
	sudo setuid 1002 rmqueue 5_user01_01.test
	showqueue
	sudo gpasswd -a tinyvm user03	
	addqueue group_user01.test
	showqueue
	rmqueue 1_group_user01.test
	sudo gpasswd -d tinyvm user03
	@rm -f *.test # delete test files
	@rmdir not_a_file

gen_test:
	@mkdir not_a_file
	@touch user01_r.test && chmod 444 user01_r.test && sudo chown user01 user01_r.test
	@touch user01_w.test && chmod 222 user01_w.test && sudo chown user01 user01_w.test
	@touch user01_x.test && chmod 111 user01_x.test && sudo chown user01 user01_x.test
	@touch group_user01.test && chmod 740 group_user01.test && sudo chown user03 group_user01.test
	@touch user01_01.test && chmod 700 user01_01.test && sudo chown user01 user01_01.test # generate files for user01 (700)
	@touch user02_02.test && chmod 700 user02_02.test && sudo chown user02 user02_02.test # generate files for user02 (700)
	@touch user03_03.test && chmod 700 user03_03.test && sudo chown user03 user03_03.test # generate files for user03 (700)
	@touch world01_01.test && sudo chown user01 world01_01.test # generate file for user01 (755)
	@touch world02_01.test && sudo chown user02 world02_01.test # generate file for user02 (755)
	@touch world03_01.test && sudo chown user03 world03_01.test # generate file for user03 (755)
