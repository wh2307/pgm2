#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>

#define SPOOLDIR "wh2307-spool" 
#define DELIM "zqz"

void die(char *message) {
perror(message);
exit(1);

}

int main(int argc, char** argv)
{
    struct dirent **list;
    char *path=NULL;
    char *lastslash=NULL;
    int i, n;   
 
    // get path
    path = strdup(argv[0]);
    lastslash = strrchr(path,'/');
    if (lastslash != NULL)
        *lastslash = 0;
    else
        *path = 0;

    char *spoolpath = (char *) calloc(sizeof(char), strlen(path) + strlen(SPOOLDIR) + 2);
    strcat(spoolpath, path);
    strcat(spoolpath, "/");
    strcat(spoolpath, SPOOLDIR);

    n = scandir(spoolpath, &list, 0, alphasort);

    for (i = 0; i < n; i++) { 
	if (strcmp(list[i]->d_name, ".") != 0 && strcmp(list[i]->d_name, "..") != 0) {
	    char *filename = strdup(list[i]->d_name);
	   
	    char *timestamp = strtok(filename, DELIM);
	    char *uid = strtok(NULL, DELIM);
	    char *fileid = strtok(NULL, DELIM);
	    char *identifier = strtok(NULL, DELIM);
 
	    printf("%s %s %s %s\n", fileid, uid, timestamp, identifier);

	    free(filename);
	    free(list[i]);
	}
    }

    free(list);
    free(path);
    free(spoolpath);

    return 0;
}
