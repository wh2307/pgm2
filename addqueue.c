#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <libgen.h>

#define MAXBUFFER 4096
#define SPOOLDIR "/wh2307-spool"
#define COUNTER "/counter_spool"
#define MAXDATE 100
#define DELIM "zqz"

void increment_counter() {
    int i;
    FILE *file = fopen(COUNTER, "r");
    fscanf(file, "%d", &i);
    fclose(file);

    ++i;

    FILE *new = fopen(COUNTER, "w");
    fprintf(new, "%d", i);
    fclose(new);
}

int get_counter() {
    int i;

    FILE *file = fopen(COUNTER, "r");
fscanf(file, "%d", &i);
    fclose(file);

    return i;
}

void die(char *message) {
    perror(message);
    exit(1);
}

int main(int argc, char **argv) {
    int i;
    int j;

    if (argc < 2) {
	    printf("usage: %s %s\n", argv[0], "<filenames to add>");
	    exit(1);
    }

    for (i = 1; i < argc; i++) {
	if (access(argv[i], R_OK)) {
	    printf("%s: X %s\n", argv[i], strerror(errno));
	}
	else {
	    char buffer[MAXBUFFER];
	    char timef[MAXDATE];
	    char usec[8];
	    char uid[8];
	    char id_append[8];
	    char *filename = strdup(argv[i]);	   

	    // if the filename includes a path, replace the / with .
	    for (j = 0; j < strlen(filename); j++) {
		if (filename[j] == '/') {
		    filename[j] = '.';
		}
	    }
 
	    // build the timestamp
	    time_t raw;time(&raw);
	    struct tm *thetime = gmtime(&raw);
	    strftime(timef, MAXDATE, "%m-%d-%Y.%X", thetime);

	    srand(time(NULL));
	    // build the arbitrary filename and UID, convert them to a string.
	    snprintf(usec, 8, "%d", get_counter());	  
	    snprintf(id_append, 8, "%05d", rand() % 100000);
	    snprintf(uid, 8, "%d", getuid()); 

	    // build the path and filename to the spool directory.
	    char *filespool = (char *) calloc((strlen(SPOOLDIR) + 1 + strlen(id_append) + 3 + strlen(uid) + 3 + strlen(timef) + 3 + strlen(usec) + 1 + strlen(argv[i]) + 1), sizeof(char));
	    strcat(strcat(strcat(strcat(strcat(strcat(strcat(strcat(strcat(strcat(strcat(filespool, SPOOLDIR), "/"), timef), DELIM), uid), DELIM), id_append), DELIM), usec), "_"), filename);
	    
	    // open the file for reading
	    int src_fd = open(argv[i], O_RDONLY);

	    // create the file in the spool directory
	    int dst_fd = open(filespool, O_CREAT | O_WRONLY, S_IWUSR);
	    	   
	    if (src_fd == -1 || dst_fd == -1) {
		printf("%s: X %s\n", argv[i], strerror(errno));
		free(filespool);
		continue;
	    }
 
	    // copy the contents to the file in the spool directory.
	    while (1) {
		int n = read(src_fd, buffer, MAXBUFFER);
		if (n == -1) { 
		    unlink(filespool);
			close(src_fd);
			close(dst_fd);
			printf("%s: X %s\n", argv[i], strerror(errno));
			break;
		}

		if (n == 0)
			close(src_fd);
			close(dst_fd); 
		    break;

		n = write(dst_fd, buffer, n);
		if (n == -1) {
		    unlink(filespool);
			close(src_fd);
			close(dst_fd);
			printf("%s: X %s\n", argv[i], strerror(errno));
			break;
		}
	     }

	    chmod(filespool, S_IWUSR);

	    free(filespool);
	    increment_counter();
	    printf("%s: Y %s_%s\n", argv[i], usec, filename);
	}

	sleep(1);
    }

    return 0;
}
