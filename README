Name: William Hom
UNI : wh2307
Assignment 2

VM info is stored in vminfo.txt (in the same directory as this README file)

Usage
-----------------

Running make will build all three programs. It will be created by whoever is 
the user running make. In addition, all three programs will be setuid programs.
Running make will compile all three programs, but nothing else. No spool
directory exists at this point. In order to run any programs, you'll need
to manually create the directory.

To install the files, you can run "make install." This will build the files 
(if not already built) and place them the into the /bin directory. It will 
also create a folder called wh2307-spool in the / directory. Finally, a counter
file called "counter_spool" is also placed in the / directory. Note that 
you must have root privileges (possible through sudo) in order to execute 
this command. Therefore, user01, user02, and user03 of the VM will be able 
to run make, but they will NOT be be able to run make install.

When installing, user01 will be set as the owner of the programs and the spool
directory.

"make gen_test" will create a bunch of test files with various permissions and
owners. "make test", in addition to createing the files, also run a serioues of
commands. Note that by installing "super" we can call setuid to simulate another
user.

To get rid of the files, spool directory, and files in the spool directory,
you can run "make uninstall". This essentially resets the environment.

A user must have read permissions on the file in order to add it to the queue.
If a user tries to print N files, and some of them cannot be printed due to
permissions errors, then those files will not be printed. For example, if
user01 prints 5 files but only has read permissions to 3, then only the 3
files will be printed.

A user cannot remove any file printed from the queue without running rmqueue.

No other user can directly view the spool directory without running showqueue.

The format of the file names consists of:

1. An arbitrary number - This is a random number seeded by the current timestamp.
2. The user ID that requested te file to be printed.
3. The timestamp in mm-dd-yyyy.hh:mm:ss format.
4. The filename with the ID with a counter pre-fixed to it. The counter ensures
that the identifier will always be unique. addqueue increments it while rmqueue
decrements it.

Each portion is separated by a "zqz". Therefore, any attempts to print a
filename with "zqz"  will throw off the file formatting of showqueue and rmqueue.

If the file you want to print has a path in it (e.g. "user01/readme.txt",
then the program will replace all "/" with a "." in the identifier. So the
command "addqueue user01/readme.txt" will have "user01.readme.txt" as its
identifier.

showqueue lists all the contents of the spool directory in no particular
order.

rmqueue will delete files based on the identifier provided. It accomplishes
this by tokenizing the filename and then comparing the last one. It then
makes sure that the UID matches with the caller as well. Both conditions
must be satisfied.

rmqueue will ALWAYS loop through all files in the spool directory until a
match in found. This is coded so that if user01 and user02 both print the 
same file, rmqueue called by either of them is guaranteed to delete the 
right file. if no match is found, then an error will be printed, but all
arguments will be looped through.

Analysis
------------------

What's happening here is that when tinyvm (the sudo user) installs the print
spooler, it makes user01 the owner of all files and directories, essentially
making it the "administrator" of the print spooler. While it is able to view
all files in the spool directory, it doesn't have any read permissions on the
files. It can, however, delete them at will.

The spool directory is set to 700 as user01 will need one of those permissions
at some point while running one of the programs. This makes backdoor attacks
possible if one of our programs has a flaw; an attacker that is able to launch
a shell through them would essentially take over the printer spool, which is
why we must program them to prevent such behavior. The file counter_spool will
also have a similar permission. We don't make either the spool directory or
counter setuid because there's no need for anyone to access them at all except
through the three programs, which are setuid. So in keeping with the principle
of least privilege, we don't put them as setuid.

The files that are copied are set to 600 permissions. We can get away with 
setting the permissions to 200 because this will still allow us to run rmqueue
and showqueue. A real print spooler, however, will eventually need to read the
contents of the file in order to print them. So while a 200 permission would be
supportive of the principle of least privilege, I opted to set permissions to
600 instead for files in the spool directory.

All three programs are set with 111 permissions. Since the files are binary,
we probably don't need to worry about users reading or writing to them, but
again, this is in keeping with the principle of least privilege.

addqueue increments the counter in the counter_spool file, while rmqueue
decrements it.
