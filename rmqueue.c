#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>

#define SPOOLDIR "/wh2307-spool"
#define COUNTER "/counter_spool"
#define DELIM "zqz"

void die(char *message) {
    perror(message);
    exit(1);
}

void decrement_counter() {
    int i;
    FILE *file = fopen(COUNTER, "r");
    fscanf(file, "%d", &i);
    fclose(file);

    --i;

    FILE *new = fopen(COUNTER, "w");
    fprintf(new, "%d", i);
    fclose(new);
}

int main(int argc, char **argv) {
    int i;
    DIR *mydir;
    char s_uid[8];

    if (argc < 2) {
	printf("usage: %s <list of file identifiers> \n", argv[0]);
	exit(1);
    }


    snprintf(s_uid, 8, "%d", getuid());

    for (i = 1; i < argc; i++) {
	struct dirent *myfile;
	struct stat mystat;
	char *uid; char *identifier, *filename;
	int found = 0; 

    if ((mydir = opendir(SPOOLDIR)) == NULL)
	die("Failed to access spool directory");

    	while((myfile = readdir(mydir)) != NULL) {
	    stat(myfile->d_name, &mystat);
	    if (strcmp(myfile->d_name, ".") != 0 && strcmp(myfile->d_name, "..") != 0) {
		filename = strdup(myfile->d_name);

		identifier = strtok(filename, DELIM); //arbitrary name
		uid = strtok(NULL, DELIM); // user ID
		identifier = strtok(NULL, DELIM); // timestamp
		identifier = strtok(NULL, DELIM); // unique identifier
		
		// if BOTH the identifier and UID are matched, then we delete the file.
		if (strcmp(argv[i], identifier) == 0) {
		    found = 1;
		    if (strcmp(uid, s_uid) == 0) {		    
			char *to_delete = calloc(sizeof(char), strlen(SPOOLDIR) + 1 + strlen(myfile->d_name) + 1);			
			strcat(strcat(strcat(to_delete, SPOOLDIR), "/"), myfile->d_name);
			unlink(to_delete);
			printf("%s: Y\n", identifier);
			free(to_delete);
			decrement_counter();
		    }
		    else {
		        printf("%s: X Operation not permitted (caller UID: %s, original UID: %s)\n", argv[i], s_uid, uid);
		    }
		}

		free(filename);

		if (found)
		    break;
	    }
        }

	if (!found) // if we loop through all the files in the directory and can't find a UID and identifier match, we display an error...
	    printf("%s: X Identifier not found\n", argv[i]);
    }

    return 0;
}
